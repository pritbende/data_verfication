package com.dmx.keyword

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable
import java.util.Random as Random

public class myKeyword {

	/**
	 * Get percentage variation between Postgres and Mongo
	 * @param postgresCount Count from Postgres DB
	 * @param mongoCount Count from MondoDB
	 * @return Variation in percentage
	 */
	@Keyword
	def getPercentageVariation(def postgresCount, def mongoCount) {

		def percentVariation = ((postgresCount - mongoCount) * 100) / postgresCount

		return percentVariation
	}

	@Keyword
	def randomNumber(def minNumber, def maxNumber) {

		Random rand = new Random()

		randomNumber = (rand.nextInt() * (maxNumber - minNumber)) + minNumber

		return randomNumber
	}
}
