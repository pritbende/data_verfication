<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description>This api will fetch top tech product list based on search criteria</description>
   <name>Firmographics</name>
   <tag></tag>
   <elementGuidId>b1f77137-1a93-45c2-8957-8de4ee29f1ba</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <connectionTimeout>-1</connectionTimeout>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;contentType&quot;: &quot;multipart/form-data&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;,
  &quot;parameters&quot;: [
    {
      &quot;name&quot;: &quot;request&quot;,
      &quot;value&quot;: &quot;{\&quot;domainList\&quot;:[\&quot;${domainName}\&quot;],\&quot;searchFilter\&quot;:{\&quot;categoryId\&quot;:[],\&quot;subcategoryId\&quot;:[],\&quot;activelyUsed\&quot;:false},\&quot;paginationAttributes\&quot;:{\&quot;pageSize\&quot;:\&quot;50\&quot;,\&quot;pageNumber\&quot;:\&quot;1\&quot;}}&quot;,
      &quot;type&quot;: &quot;Text&quot;,
      &quot;contentType&quot;: &quot;&quot;
    }
  ]
}</httpBodyContent>
   <httpBodyType>form-data</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>${jwt_token}</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>multipart/form-data</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Accept</name>
      <type>Main</type>
      <value>*/*</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Accept-Encoding</name>
      <type>Main</type>
      <value>gzip, deflate, br</value>
   </httpHeaderProperties>
   <katalonVersion>7.5.1</katalonVersion>
   <maxResponseSize>-1</maxResponseSize>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>${url}/${url_suffix_api}/${endpoint_firmographics}</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <socketTimeout>-1</socketTimeout>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <variables>
      <defaultValue>GlobalVariable.url</defaultValue>
      <description></description>
      <id>fdd88b4d-5063-4546-8d82-6ad8661c6ca1</id>
      <masked>false</masked>
      <name>url</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.url_suffix_api</defaultValue>
      <description></description>
      <id>2c1f6c53-3a45-4d91-a536-60b4b7972708</id>
      <masked>false</masked>
      <name>url_suffix_api</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.endpoint_firmographics</defaultValue>
      <description></description>
      <id>159f2405-0f65-468b-8053-cd7f0ad9a732</id>
      <masked>false</masked>
      <name>endpoint_firmographics</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.token</defaultValue>
      <description></description>
      <id>f4f47575-d016-4b16-9d88-1b3af3c109a9</id>
      <masked>false</masked>
      <name>jwt_token</name>
   </variables>
   <variables>
      <defaultValue>'sourcebooks.com'</defaultValue>
      <description></description>
      <id>f42779d1-d758-4a2b-ad9e-f2b7d5f72940</id>
      <masked>false</masked>
      <name>domainName</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()


WS.verifyResponseStatusCode(response, 200)

assertThat(response.getStatusCode()).isEqualTo(200)</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
