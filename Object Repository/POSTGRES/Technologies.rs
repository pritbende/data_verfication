<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description>This api will fetch top tech product list based on search criteria</description>
   <name>Technologies</name>
   <tag></tag>
   <elementGuidId>5f1a75e1-dfda-422b-ba1b-9875a097dd34</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <connectionTimeout>-1</connectionTimeout>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\n    \&quot;technologyId\&quot;: [\n        ${techId}\n    ],\n    \&quot;companyId\&quot;: 0,\n    \&quot;searchFilter\&quot;: {\n        \&quot;countryId\&quot;: [${countryId}],\n        \&quot;industryId\&quot;: [${industryId}],\n        \&quot;revenueId\&quot;: [${revenueId}],\n        \&quot;sizeId\&quot;: [${sizeId}],\n        \&quot;intensityId\&quot;: [${intensityId}],\n        \&quot;activelyUsed\&quot;: false,\n        \&quot;firstFound\&quot;: \&quot;\&quot;,\n        \&quot;lastFound\&quot;: \&quot;\&quot;\n    },\n    \&quot;paginationAttributes\&quot;: {\n        \&quot;pageSize\&quot;: \&quot;50\&quot;,\n        \&quot;pageNumber\&quot;: \&quot;1\&quot;\n    }\n}&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>${jwt_token}</value>
   </httpHeaderProperties>
   <katalonVersion>7.5.1</katalonVersion>
   <maxResponseSize>-1</maxResponseSize>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>${url}/${url_suffix_api}/${endpoint_technologies}</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <socketTimeout>-1</socketTimeout>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <variables>
      <defaultValue>GlobalVariable.url</defaultValue>
      <description></description>
      <id>3e9c1158-b738-4f08-b3d3-977a14645671</id>
      <masked>false</masked>
      <name>url</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.url_suffix_api</defaultValue>
      <description></description>
      <id>31249568-0044-4861-aade-e38be4bc3561</id>
      <masked>false</masked>
      <name>url_suffix_api</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.endpoint_technologies</defaultValue>
      <description></description>
      <id>159f2405-0f65-468b-8053-cd7f0ad9a732</id>
      <masked>false</masked>
      <name>endpoint_technologies</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.token</defaultValue>
      <description></description>
      <id>f4f47575-d016-4b16-9d88-1b3af3c109a9</id>
      <masked>false</masked>
      <name>jwt_token</name>
   </variables>
   <variables>
      <defaultValue>'2454'</defaultValue>
      <description></description>
      <id>d787616b-0916-4d39-a8a1-d887d1b38017</id>
      <masked>false</masked>
      <name>techId</name>
   </variables>
   <variables>
      <defaultValue>'210'</defaultValue>
      <description></description>
      <id>16afa0e1-202b-421c-82a8-80522c470630</id>
      <masked>false</masked>
      <name>countryId</name>
   </variables>
   <variables>
      <defaultValue>''</defaultValue>
      <description></description>
      <id>f0ba54b7-53d9-4439-88e9-86e4a3d6d245</id>
      <masked>false</masked>
      <name>industryId</name>
   </variables>
   <variables>
      <defaultValue>''</defaultValue>
      <description></description>
      <id>9e82bb5c-af6f-4682-8dca-3999bd177e06</id>
      <masked>false</masked>
      <name>intensityId</name>
   </variables>
   <variables>
      <defaultValue>''</defaultValue>
      <description></description>
      <id>5a6ee819-7314-46d0-a0ee-ef3f2a892272</id>
      <masked>false</masked>
      <name>revenueId</name>
   </variables>
   <variables>
      <defaultValue>''</defaultValue>
      <description></description>
      <id>fc9abd9f-dbc1-4d6d-87d1-4b5fc7afc41b</id>
      <masked>false</masked>
      <name>sizeId</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()


WS.verifyResponseStatusCode(response, 200)

assertThat(response.getStatusCode()).isEqualTo(200)</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
