<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description>This api will fetch top tech product list based on search criteria</description>
   <name>Product</name>
   <tag></tag>
   <elementGuidId>f310ef0f-be2d-485a-b698-3329bcc41c68</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <connectionTimeout>-1</connectionTimeout>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent></httpBodyContent>
   <httpBodyType></httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>${jwt_token}</value>
   </httpHeaderProperties>
   <katalonVersion>7.5.1</katalonVersion>
   <maxResponseSize>-1</maxResponseSize>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>GET</restRequestMethod>
   <restUrl>${url_mongo}/${url_suffix_api}/${endpoint_products}</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <socketTimeout>-1</socketTimeout>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <variables>
      <defaultValue>GlobalVariable.url_mongo</defaultValue>
      <description></description>
      <id>3e9c1158-b738-4f08-b3d3-977a14645671</id>
      <masked>false</masked>
      <name>url_mongo</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.url_suffix_api</defaultValue>
      <description></description>
      <id>31249568-0044-4861-aade-e38be4bc3561</id>
      <masked>false</masked>
      <name>url_suffix_api</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.endpoint_products</defaultValue>
      <description></description>
      <id>159f2405-0f65-468b-8053-cd7f0ad9a732</id>
      <masked>false</masked>
      <name>endpoint_products</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.token_gcp</defaultValue>
      <description></description>
      <id>f4f47575-d016-4b16-9d88-1b3af3c109a9</id>
      <masked>false</masked>
      <name>jwt_token</name>
   </variables>
   <variables>
      <defaultValue>'5fa2fe5e2f49df5e0b02ba29'</defaultValue>
      <description></description>
      <id>d787616b-0916-4d39-a8a1-d887d1b38017</id>
      <masked>false</masked>
      <name>techId_mongo</name>
   </variables>
   <variables>
      <defaultValue>''</defaultValue>
      <description></description>
      <id>16afa0e1-202b-421c-82a8-80522c470630</id>
      <masked>false</masked>
      <name>countryId</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()


WS.verifyResponseStatusCode(response, 200)

assertThat(response.getStatusCode()).isEqualTo(200)</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
