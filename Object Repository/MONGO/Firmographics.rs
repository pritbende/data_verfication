<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description>This api will fetch top tech product list based on search criteria</description>
   <name>Firmographics</name>
   <tag></tag>
   <elementGuidId>735c777f-61cf-41c5-b434-239753e9bad0</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <connectionTimeout>-1</connectionTimeout>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;contentType&quot;: &quot;multipart/form-data&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;,
  &quot;parameters&quot;: [
    {
      &quot;name&quot;: &quot;request&quot;,
      &quot;value&quot;: &quot;{\&quot;domainList\&quot;:  [\&quot;${domain}\&quot;],\&quot;searchFilter\&quot;: {\&quot;categoryId\&quot;: [],\&quot;subcategoryId\&quot;: [],\&quot;activelyUsed\&quot;: false},\&quot;paginationAttributes\&quot;: {\&quot;pageSize\&quot;: \&quot;50\&quot;,\&quot;pageNumber\&quot;: \&quot;1\&quot;}}&quot;,
      &quot;type&quot;: &quot;Text&quot;,
      &quot;contentType&quot;: &quot;multipart/form-data&quot;
    }
  ]
}</httpBodyContent>
   <httpBodyType>form-data</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>multipart/form-data</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>${jwt_token}</value>
   </httpHeaderProperties>
   <katalonVersion>7.5.1</katalonVersion>
   <maxResponseSize>-1</maxResponseSize>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>${url}/${url_suffix_api}/${endpoint_firmographics}</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <socketTimeout>-1</socketTimeout>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <variables>
      <defaultValue>GlobalVariable.url_mongo</defaultValue>
      <description></description>
      <id>fdd88b4d-5063-4546-8d82-6ad8661c6ca1</id>
      <masked>false</masked>
      <name>url</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.url_suffix_api</defaultValue>
      <description></description>
      <id>2c1f6c53-3a45-4d91-a536-60b4b7972708</id>
      <masked>false</masked>
      <name>url_suffix_api</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.endpoint_firmographics</defaultValue>
      <description></description>
      <id>159f2405-0f65-468b-8053-cd7f0ad9a732</id>
      <masked>false</masked>
      <name>endpoint_firmographics</name>
   </variables>
   <variables>
      <defaultValue>'eyJraWQiOiJIRllwSm5JdlNyc3MrREtVc3k3QVcrdldDZkxPRWV5TWdTS1BZK1wvUE9OYz0iLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJhOTVlNWFlMi1lYjQ1LTQ5MzYtODY2Zi0zZjQwNmU5ZTg1NWEiLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwiaXNzIjoiaHR0cHM6XC9cL2NvZ25pdG8taWRwLnVzLXdlc3QtMi5hbWF6b25hd3MuY29tXC91cy13ZXN0LTJfa1BzQmx1b0k5IiwicGhvbmVfbnVtYmVyX3ZlcmlmaWVkIjp0cnVlLCJjb2duaXRvOnVzZXJuYW1lIjoiYmhhdmVzaCIsImF1ZCI6IjMwamU3Y24xZ2NoNXRtOGRrZXIwcjVsdDh0IiwiZXZlbnRfaWQiOiI3ZTA1NGZjMi03YzhjLTQzNGYtOTUzNS1iMDkzNjdjMTE2YTciLCJ0b2tlbl91c2UiOiJpZCIsImF1dGhfdGltZSI6MTYwNjQ4MTcyMSwicGhvbmVfbnVtYmVyIjoiKzkxOTgxOTM0MDU2NSIsImV4cCI6MTYwNjQ4NTMyMSwiaWF0IjoxNjA2NDgxNzIxLCJlbWFpbCI6ImFjaGF3ZGFAZ21haWwuY29tIn0.g-xiX1j1LbuKmv5RWNaVxXAAUiGspuBJkQftHfQ5dCNYQdAdZq3mdMFrOQIWYrl7_J0jBh7qVMuhrqPUvU0mm7R_p4Yz4z0YVuh4JWIgEbfsfXrFuuxt3NmswyN2CK_vflnK9Y0YBOH2UYA48E-45BHIOnt08C3-fx5_L4PE6U7z-b-BGAZb1ufnIOumxem-nvtnjIiu0FQAlaGbV3yQng19fE0GXa0P1eg288Xx_KvTHXBR8BH0FMQxdmx_s5YeBLiNiAjczIM1JSxH4PmcxLUiJrmLkirLuQTLtZ-BgoNNKbDPeAfvsq8bqnrqHXcrfwO3IpOm-mRciib1qdIGiw'</defaultValue>
      <description></description>
      <id>f4f47575-d016-4b16-9d88-1b3af3c109a9</id>
      <masked>false</masked>
      <name>jwt_token</name>
   </variables>
   <variables>
      <defaultValue>'sourcebooks.com'</defaultValue>
      <description></description>
      <id>f42779d1-d758-4a2b-ad9e-f2b7d5f72940</id>
      <masked>false</masked>
      <name>domain</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()


WS.verifyResponseStatusCode(response, 200)

assertThat(response.getStatusCode()).isEqualTo(200)</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
