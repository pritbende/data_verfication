# Technographics data testing via API

Testing technographics data based on Product, Category/Subcategory, Industry, Intensity, Revenue & EmpSize via API

## Installation

Download KatalonStudio community edition from

```bash
https://www.katalon.com/
```

## Usage

```
To run automation test suite, go to TestSuites->TechnographicsDataTestSuite and click on Run button
```
