import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import internal.GlobalVariable as GlobalVariable

def isDataInprogress_mongo = true

def products_response

def responseMongo

Map resultMongo

def timeoutCount = 0

//'API request for MONGO'
'Get products data'
products_response = WS.sendRequest(findTestObject('MONGO/Product', [('url_mongo') : GlobalVariable.url_mongo, ('url_suffix_api') : GlobalVariable.url_suffix_api
            , ('endpoint_products') : GlobalVariable.endpoint_products, ('jwt_token') : GlobalVariable.token_gcp, ('techId_mongo') : '5fa2fe5e2f49df5e0b02ba29'
            , ('countryId') : '']))

'Get new authorization token if it is expired'
if (products_response.statusCode == 401) {
    'Call authorization api'
    WS.callTestCase(findTestCase('Authentication/GetAuthorizationToken GCP'), [:], FailureHandling.STOP_ON_FAILURE)

    'Get products data'
    products_response = WS.sendRequest(findTestObject('MONGO/Product', [('url_mongo') : GlobalVariable.url_mongo, ('url_suffix_api') : GlobalVariable.url_suffix_api
                , ('endpoint_products') : GlobalVariable.endpoint_products, ('jwt_token') : GlobalVariable.token_gcp, ('techId_mongo') : '5fa2fe5e2f49df5e0b02ba29'
                , ('countryId') : '']))
}

'Verify status code is 200'
WS.verifyResponseStatusCode(products_response, 200)

'Parse json response'
def parsedJsonMongo = new groovy.json.JsonSlurper()

'Convert JSON response to Map'
resultMongo = parsedJsonMongo.parseText(products_response.getResponseBodyContent())

KeywordUtil.logInfo('Technology Count: ' + resultMongo.get('technologyList').size())

isDataInprogress_mongo = false

Random rand = new Random()

for (int i = 1; i < 5; i++) {
    randomNumber = rand.nextInt(resultMongo.get('technologyList').size() - 1)

    def product_name = (resultMongo.get('technologyList')[randomNumber])['name']

    KeywordUtil.logInfo('Product_name: ' + product_name)

    def product_id = (resultMongo.get('technologyList')[randomNumber])['id']

    KeywordUtil.logInfo('Product_id: ' + product_id)

    //for product_id, get technographics API response and retrieve category & subcategory name
    responseTechno = WS.sendRequest(findTestObject('MONGO/Technologies_Product', [('url_mongo') : GlobalVariable.url_mongo
                , ('url_suffix_api') : GlobalVariable.url_suffix_api, ('endpoint_technologies') : GlobalVariable.endpoint_technologies
                , ('jwt_token') : GlobalVariable.token_gcp, ('techId_mongo') : product_id]))

    'Verify status code is 200'
    WS.verifyResponseStatusCode(responseTechno, 200)

    'Convert JSON response to Map'
    def resultTechno = parsedJsonMongo.parseText(responseTechno.getResponseBodyContent())

    def actual_category = (resultTechno.get('technographicsDetails')[0])['categoryName']

    def actual_subcategory = (resultTechno.get('technographicsDetails')[0])['subcategoryName']

    KeywordUtil.logInfo('Category name: ' + actual_category)

    KeywordUtil.logInfo('SubCategory name: ' + actual_subcategory)

    KeywordUtil.logInfo('totalCount: ' + resultTechno.get('totalCount'))

    //verify cateory & subcategory name using it category_id from db
    for (def var : resultTechno.get('technologyList')) {
        KeywordUtil.logInfo('var: ' + var)

        assert actual_category == (var['categoryName'])

        assert actual_subcategory == (var['subcategoryName']) // for each domain in technologyList, run firmographics API and verify its Industry, revenue, empSize, Country
    }
}

