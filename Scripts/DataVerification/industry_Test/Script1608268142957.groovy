import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.logging.KeywordLogger as KeywordLogger
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import internal.GlobalVariable as GlobalVariable

KeywordLogger log = new KeywordLogger()

def isDataInprogress_mongo = true

def master_response

def responseMongo

Map master_parsed_response

def timeoutCount = 0

Random rand = new Random()

//Get Intensity id and name from master api
//verify intensity name for each technologyList array
//'API request for MONGO'
'Get products data'
master_response = WS.sendRequest(findTestObject('MONGO/MasterData', [('url_mongo') : GlobalVariable.url_mongo, ('url_suffix_api') : GlobalVariable.url_suffix_api
            , ('endpoint_masterdata') : GlobalVariable.endpoint_masterData, ('jwt_token') : GlobalVariable.token_gcp, ('techId_mongo') : '5fa2fe5e2f49df5e0b02ba29'
            , ('countryId') : '']))

'Get new authorization token if it is expired'
if (master_response.statusCode == 401) {
    'Call authorization api'
    WS.callTestCase(findTestCase('Authentication/GetAuthorizationToken GCP'), [:], FailureHandling.STOP_ON_FAILURE)

    'Get products data'
    master_response = WS.sendRequest(findTestObject('MONGO/MasterData', [('url_mongo') : GlobalVariable.url_mongo, ('url_suffix_api') : GlobalVariable.url_suffix_api
                , ('endpoint_masterdata') : GlobalVariable.endpoint_masterData, ('jwt_token') : GlobalVariable.token_gcp
                , ('techId_mongo') : '5fa2fe5e2f49df5e0b02ba29', ('countryId') : '']))
}

'Verify status code is 200'
WS.verifyResponseStatusCode(master_response, 200)

'Parse json response'
def parsedJsonMongo = new groovy.json.JsonSlurper()

'Convert JSON response to Map'
master_parsed_response = parsedJsonMongo.parseText(master_response.getResponseBodyContent())

def industry_map = [:]

for(def arr : master_parsed_response.get('industryList')) {
	
	industry_map.put(arr['name'], arr['id'])
}

KeywordUtil.logInfo('MasterData as map: ' + industry_map)

//get random products with intensity filter from product api
products_response = WS.sendRequest(findTestObject('MONGO/Product', [('url_mongo') : GlobalVariable.url_mongo, ('url_suffix_api') : GlobalVariable.url_suffix_api
            , ('endpoint_products') : GlobalVariable.endpoint_products, ('jwt_token') : GlobalVariable.token_gcp]))

'Convert JSON response to Map'
def product_parsed_response = parsedJsonMongo.parseText(products_response.getResponseBodyContent())

for (int i = 1; i <= GlobalVariable.productsNum; i++) {
    randomNumber = rand.nextInt(product_parsed_response.get('technologyList').size() - 1)

    def product_name = (product_parsed_response.get('technologyList')[randomNumber])['name']

    KeywordUtil.logInfo('Product_name: ' + product_name)

    productId = ((product_parsed_response.get('technologyList')[randomNumber])['id'])

    KeywordUtil.logInfo('Product_id: ' + productId)

    'Get industry list against product'
    def industry_response = WS.sendRequest(findTestObject('MONGO/Technologies_Product', [('url_mongo') : GlobalVariable.url_mongo
                , ('url_suffix_api') : GlobalVariable.url_suffix_api, ('endpoint_technologies') : GlobalVariable.endpoint_technologies
                , ('jwt_token') : GlobalVariable.token_gcp, ('techId_mongo') : productId]))

    //get industry list of product
    def industry_parsed_response = parsedJsonMongo.parseText(industry_response.getResponseBodyContent())

    KeywordUtil.logInfo('IndustryList: ' + industry_parsed_response.get('technologyList'))
	
    def industry_present = []

    for (def ind_ele : industry_parsed_response.get('technologyList')) {
        if (!(industry_present.contains(ind_ele['industry'])) && ind_ele['industry'] != null) {
			
            industry_present.add(ind_ele['industry'])
        }
    }
	
	KeywordUtil.logInfo('Size of array: ' + industry_present)
	def terminator = 0
	//get industry id and send product + industry filter api
	for(def industry : industry_present) {
		
		if(++terminator == 5) {
			break
			terminator = 0
		}
		
		if(industry_map.containsKey(industry)) {
			
			industryId = industry_map.get(industry)
			
			KeywordUtil.logInfo('industryName:' + industryId + ' ' + industry)
			
			//for product_id, get technographics API response and retrieve category & subcategory name
			responseTechno = WS.sendRequest(findTestObject('MONGO/Technologies_Industry', [('url_mongo') : GlobalVariable.url_mongo
						, ('url_suffix_api') : GlobalVariable.url_suffix_api, ('endpoint_technologies') : GlobalVariable.endpoint_technologies
						, ('jwt_token') : GlobalVariable.token_gcp, ('techId_mongo') : productId, ('industryId') : industryId]))
	
			'Verify status code is 200'
			WS.verifyResponseStatusCode(responseTechno, 200)
	
			'Convert JSON response to Map'
			def resultTechno = parsedJsonMongo.parseText(responseTechno.getResponseBodyContent())
	
			KeywordUtil.logInfo('techno response: ' + resultTechno)
	
			//verify cateory & subcategory name using it category_id from db
			for (def var : resultTechno.get('technologyList')) {
				
				KeywordUtil.logInfo('var: ' + var)
	
				assert industry == (var['industry'])
			}
		}
		
		else {
			KeywordUtil.logInfo("INdustry name and id mapping not found: " + industry)
			KeywordUtil.markWarning("INdustry name and id mapping not found.")
		}
	}
		
			
}