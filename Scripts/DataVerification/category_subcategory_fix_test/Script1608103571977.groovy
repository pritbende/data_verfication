import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import internal.GlobalVariable as GlobalVariable

def isDataInprogress_mongo = true

def responseMongo

Map resultMongo

def timeoutCount = 0

//'API request for MONGO'
while (isDataInprogress_mongo) {
    'API request'
    responseMongo = WS.sendRequest(findTestObject('MONGO/Technologies_Product', [('url_mongo') : GlobalVariable.url_mongo
                , ('url_suffix_api') : GlobalVariable.url_suffix_api, ('endpoint_technologies') : GlobalVariable.endpoint_technologies
                , ('jwt_token') : GlobalVariable.token_gcp, ('techId_mongo') : techId_mongo]))

    'Get new authorization token if it is expired'
    if (responseMongo.statusCode == 401) {
        'Call authorization api'
        WS.callTestCase(findTestCase('Authentication/GetAuthorizationToken GCP'), [:], FailureHandling.STOP_ON_FAILURE)

        'Send API request to postgres'
        responseMongo = WS.sendRequest(findTestObject('MONGO/Technologies_Product', [('url_mongo') : GlobalVariable.url_mongo
                    , ('url_suffix_api') : GlobalVariable.url_suffix_api, ('endpoint_technologies') : GlobalVariable.endpoint_technologies
                    , ('jwt_token') : GlobalVariable.token_gcp, ('techId_mongo') : techId_mongo]))
    }
    
    'Verify status code is 200'
    WS.verifyResponseStatusCode(responseMongo, 200)

    'Parse json response'
    def parsedJsonMongo = new groovy.json.JsonSlurper()

    'Convert JSON response to Map'
    resultMongo = parsedJsonMongo.parseText(responseMongo.getResponseBodyContent())

    'Wait for few seconds if data preparation is INPROGRESS and send the request again'
    if (resultMongo.get('responseCode').equals('INPROGRESS')) {
        'Mark test case FAILED if time out for inprogress data is reached'
        if (++timeoutCount > GlobalVariable.inProgressTimeOut) {
            KeywordUtil.markFailedAndStop('Time out reached waiting for inprogress data preparation to get complete: ' + 
                resultMongo.get('responseMessage'))
        } else {
            'Wait for 20 sec'
            KeywordUtil.logInfo(('Data preparation is inprogress. Waiting for ' + GlobalVariable.sleepTime) + ' seconds')

            Thread.sleep(GlobalVariable.sleepTime)
        }
    } else if (resultMongo.get('responseCode').equals('OK')) {
        KeywordUtil.logInfo('Data preparation is completed')

        isDataInprogress_mongo = false
    } else if (resultMongo.get('responseCode').equals('ERROR')) {
        KeywordUtil.markFailedAndStop(resultMongo.get('responseMessage'))
    }
}

def actual_category = (resultMongo.get('technographicsDetails')[0])['categoryName']

def actual_subcategory = (resultMongo.get('technographicsDetails')[0])['subcategoryName']

'TestCase: Verify category name'
assert actual_category == category_name

'TestCase: Verify subcategory name'
assert actual_subcategory == subcategory_name

'TestCase: Verify category & subcategory from technologyList'
for (def var : resultMongo.get('technologyList')) {
    assert (var['categoryName']) == category_name

    assert (var['subcategoryName']) == subcategory_name
}

