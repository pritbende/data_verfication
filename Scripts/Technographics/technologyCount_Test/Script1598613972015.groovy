import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.dmx.keyword.myKeyword as myKeyword

def isDataInprogress = true

def responsePostgres

Map resultPostgres

def timeoutCount = 0

while (isDataInprogress) {
    'API request for Postgres'
    responsePostgres = WS.sendRequest(findTestObject('POSTGRES/Technologies', [('url') : GlobalVariable.url, ('url_suffix_api') : GlobalVariable.url_suffix_api
                , ('endpoint_technologies') : GlobalVariable.endpoint_technologies, ('jwt_token') : GlobalVariable.token
                , ('techId') : techId, ('countryId') : countryId, ('industryId') : industryId, ('intensityId') : intensityId
                , ('revenueId') : revenueId, ('sizeId') : sizeId]))

    'Get new authorization token if it is expired'
    if (responsePostgres.statusCode == 401) {
        'Call authorization api'
        WS.callTestCase(findTestCase('Authentication/GetAuthorizationToken'), [('token') : ''], FailureHandling.STOP_ON_FAILURE)

        'Send API request to postgres'
        responsePostgres = WS.sendRequest(findTestObject('POSTGRES/Technologies', [('url') : GlobalVariable.url, ('url_suffix_api') : GlobalVariable.url_suffix_api
                    , ('endpoint_technologies') : GlobalVariable.endpoint_technologies, ('jwt_token') : GlobalVariable.token
                    , ('techId') : techId, ('countryId') : countryId, ('industryId') : industryId, ('intensityId') : intensityId
                    , ('revenueId') : revenueId, ('sizeId') : sizeId]))
    }
    
    'Verify status code is 200'
    WS.verifyResponseStatusCode(responsePostgres, 200)

    'Parse json response'
    def parsedJsonPostgres = new groovy.json.JsonSlurper()

    'Convert JSON response to Map'
    resultPostgres = parsedJsonPostgres.parseText(responsePostgres.getResponseBodyContent())

    'Wait for few seconds if data preparation is INPROGRESS and send the request again'
    if (resultPostgres.get('responseCode').equals('INPROGRESS')) {
        'Mark test case FAILED if time out for inprogress data is reached'
        if (++timeoutCount > GlobalVariable.inProgressTimeOut) {
            KeywordUtil.markFailedAndStop('Time out reached waiting for inprogress data preparation to get complete: ' + 
                resultPostgres.get('responseMessage'))
        } else {
            'Wait for 20 sec'
            KeywordUtil.logInfo(('Data preparation is inprogress. Waiting for ' + GlobalVariable.sleepTime) + ' seconds')

            Thread.sleep(GlobalVariable.sleepTime)
        }
    } else if (resultPostgres.get('responseCode').equals('OK')) {
        KeywordUtil.logInfo('Data preparation is completed')

        isDataInprogress = false
    } else if (resultPostgres.get('responseCode').equals('ERROR')) {
        KeywordUtil.markFailedAndStop(resultPostgres.get('responseMessage'))
    }
}

'Get value of totalCount Postgres'
def totalCountPostgres = resultPostgres.get('totalCount')

KeywordUtil.logInfo('Postgres Total Count: ' + totalCountPostgres)

def isDataInprogress_mongo = true

def responseMongo

Map resultMongo

//'API request for MONGO'
while (isDataInprogress_mongo) {
    'API request for Postgres'
    responseMongo = WS.sendRequest(findTestObject('MONGO/Technologies_Intensity', [('url_mongo') : GlobalVariable.url_mongo, ('url_suffix_api') : GlobalVariable.url_suffix_api
                , ('endpoint_technologies') : GlobalVariable.endpoint_technologies, ('jwt_token') : GlobalVariable.token_gcp
                , ('techId_mongo') : techId_mongo, ('countryId') : '']))

    'Get new authorization token if it is expired'
    if (responseMongo.statusCode == 401) {
        'Call authorization api'
        WS.callTestCase(findTestCase('Authentication/GetAuthorizationToken GCP'), [:], FailureHandling.STOP_ON_FAILURE)

        'Send API request to postgres'
        responseMongo = WS.sendRequest(findTestObject('MONGO/Technologies_Intensity', [('url_mongo') : GlobalVariable.url_mongo, ('url_suffix_api') : GlobalVariable.url_suffix_api
                    , ('endpoint_technologies') : GlobalVariable.endpoint_technologies, ('jwt_token') : GlobalVariable.token_gcp
                    , ('techId_mongo') : techId_mongo, ('countryId') : '']))
    }
    
    'Verify status code is 200'
    WS.verifyResponseStatusCode(responseMongo, 200)

    'Parse json response'
    def parsedJsonMongo = new groovy.json.JsonSlurper()

    'Convert JSON response to Map'
    resultMongo = parsedJsonMongo.parseText(responseMongo.getResponseBodyContent())

    'Wait for few seconds if data preparation is INPROGRESS and send the request again'
    if (resultMongo.get('responseCode').equals('INPROGRESS')) {
        'Mark test case FAILED if time out for inprogress data is reached'
        if (++timeoutCount > GlobalVariable.inProgressTimeOut) {
            KeywordUtil.markFailedAndStop('Time out reached waiting for inprogress data preparation to get complete: ' + 
                resultMongo.get('responseMessage'))
        } else {
            'Wait for 20 sec'
            KeywordUtil.logInfo(('Data preparation is inprogress. Waiting for ' + GlobalVariable.sleepTime) + ' seconds')

            Thread.sleep(GlobalVariable.sleepTime)
        }
    } else if (resultMongo.get('responseCode').equals('OK')) {
        KeywordUtil.logInfo('Data preparation is completed')

        isDataInprogress_mongo = false
    } else if (resultMongo.get('responseCode').equals('ERROR')) {
        KeywordUtil.markFailedAndStop(resultMongo.get('responseMessage'))
    }
}

'Get value of totalCount Mongo'
def totalCountMongo = resultMongo.get('totalCount')

KeywordUtil.logInfo('Mongo Total Count: ' + totalCountMongo)

def variation = CustomKeywords.'com.dmx.keyword.myKeyword.getPercentageVariation'(totalCountPostgres, totalCountMongo)

'Mark test case PASSED/FAILED'
if ((variation < GlobalVariable.failureVariation) && (variation > -(GlobalVariable.failureVariation))) {
    KeywordUtil.markPassed('Result variation is within +-10. PASSED')
} else {
    KeywordUtil.markFailed('Result variation is out of range. FAILED')
}

