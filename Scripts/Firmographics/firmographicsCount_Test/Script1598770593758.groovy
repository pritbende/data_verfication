import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import org.eclipse.jdt.internal.compiler.ast.ForeachStatement as ForeachStatement
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil

def isDataInprogress = true

def responsePostgres

Map resultPostgres

def timeoutCount = 0

while (isDataInprogress) {
    //API request for Postgres
    responsePostgres = WS.sendRequest(findTestObject('POSTGRES/Firmographics', [('url') : GlobalVariable.url, ('url_suffix_api') : GlobalVariable.url_suffix_api
                , ('endpoint_firmographics') : GlobalVariable.endpoint_firmographics, ('jwt_token') : GlobalVariable.token
                , ('domainName') : domainName]))

    'Get new authorization token if it is expired'
    if (responsePostgres.statusCode == 401) {
        'Call authorization api'
        WS.callTestCase(findTestCase('Authentication/GetAuthorizationToken'), [('token') : ''], FailureHandling.STOP_ON_FAILURE)

        responsePostgres = WS.sendRequest(findTestObject('POSTGRES/Firmographics', [('url') : GlobalVariable.url, ('url_suffix_api') : GlobalVariable.url_suffix_api
                    , ('endpoint_firmographics') : GlobalVariable.endpoint_firmographics, ('jwt_token') : GlobalVariable.token
                    , ('domainName') : domainName]))
    }
    
    'Verify status code is 200'
    WS.verifyResponseStatusCode(responsePostgres, 200)

    'Parse json response'
    def parsedJsonPostgres = new groovy.json.JsonSlurper()

    'Convert JSON response to Map'
    resultPostgres = parsedJsonPostgres.parseText(responsePostgres.getResponseBodyContent())

    KeywordUtil.logInfo('Response: ' + responsePostgres.getResponseBodyContent())

    'Wait for few seconds if data preparation is INPROGRESS and send the request again'
    if (resultPostgres.get('responseCode').equals('INPROGRESS')) {
        'Mark test case FAILED if time out for inprogress data is reached'
        if (++timeoutCount > GlobalVariable.inProgressTimeOut) {
            KeywordUtil.markFailedAndStop('Time out reached waiting for inprogress data preparation to get complete: ' + 
                resultPostgres.get('responseMessage'))
        } else {
            'Wait for 20 sec'
            KeywordUtil.logInfo(('Data preparation is inprogress. Waiting for ' + GlobalVariable.sleepTime) + ' seconds')

            Thread.sleep(GlobalVariable.sleepTime)
        }
    } else if (resultPostgres.get('responseCode').equals('OK')) {
        KeywordUtil.logInfo('Data preparation is completed')

        isDataInprogress = false
    } else if (resultPostgres.get('responseCode').equals('ERROR')) {
        KeywordUtil.markFailedAndStop(resultPostgres.get('responseMessage'))
    }
}

'Get value of technographicsList'
Object[] technographicsCountPostgres = resultPostgres.technographicsList

KeywordUtil.logInfo('Postgres technographicsList Count: ' + technographicsCountPostgres.size())

def isDataInprogress_mongo = true

def responseMongo

Map resultMongo

while (isDataInprogress_mongo) {
    //API request for Postgres
    responseMongo = WS.sendRequest(findTestObject('MONGO/Firmographics', [('url') : GlobalVariable.url_mongo, ('url_suffix_api') : GlobalVariable.url_suffix_api
                , ('endpoint_firmographics') : GlobalVariable.endpoint_firmographics, ('jwt_token') : GlobalVariable.token_gcp
                , ('companyId') : domainName]))

    'Get new authorization token if it is expired'
    if (responseMongo.statusCode == 401) {
        'Call authorization api'
        WS.callTestCase(findTestCase('Authentication/GetAuthorizationToken GCP'), [:], FailureHandling.STOP_ON_FAILURE)

        responseMongo = WS.sendRequest(findTestObject('MONGO/Firmographics', [('url') : GlobalVariable.url_mongo, ('url_suffix_api') : GlobalVariable.url_suffix_api
                    , ('endpoint_firmographics') : GlobalVariable.endpoint_firmographics, ('jwt_token') : GlobalVariable.token_gcp
                    , ('companyId') : domainName]))
    }
    
    'Verify status code is 200'
    WS.verifyResponseStatusCode(responseMongo, 200)

    'Parse json response'
    def parsedJsonPostgres = new groovy.json.JsonSlurper()

    'Convert JSON response to Map'
    resultMongo = parsedJsonPostgres.parseText(responseMongo.getResponseBodyContent())

    KeywordUtil.logInfo('Response: ' + responseMongo.getResponseBodyContent())

    'Wait for few seconds if data preparation is INPROGRESS and send the request again'
    if (resultMongo.get('responseCode').equals('INPROGRESS')) {
        'Mark test case FAILED if time out for inprogress data is reached'
        if (++timeoutCount > GlobalVariable.inProgressTimeOut) {
            KeywordUtil.markFailedAndStop('Time out reached waiting for inprogress data preparation to get complete: ' + 
                resultMongo.get('responseMessage'))
        } else {
            'Wait for 20 sec'
            KeywordUtil.logInfo(('Data preparation is inprogress. Waiting for ' + GlobalVariable.sleepTime) + ' seconds')

            Thread.sleep(GlobalVariable.sleepTime)
        }
    } else if (resultMongo.get('responseCode').equals('OK')) {
        KeywordUtil.logInfo('Data preparation is completed')

        isDataInprogress_mongo = false
    } else if (resultMongo.get('responseCode').equals('ERROR')) {
        KeywordUtil.markFailedAndStop(resultMongo.get('responseMessage'))
    }
}

'Get value of technographicsList'
Object[] technographicsCountMongo = resultMongo.technographicsList

KeywordUtil.logInfo('Mongo technographicsList Count: ' + technographicsCountMongo.size())